import Vue from 'vue'
import BaseButton from '~/components/global/Buttons/base-button.vue'

const components = { BaseButton }

Object.entries(components).forEach(([name, component]) => {
    Vue.component(name, component)
})