import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const page = path => () => import(`~/components/${path}`).then(m => m.default || m)

export const routes = [
  { path: '/', name: 'Home', component: page('Home') },
]

export function createRouter() {
  return new Router({
    mode: 'history',
    routes
  })
}
