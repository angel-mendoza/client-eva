export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'client-eva',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    '~plugins/bl-components'
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module'
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/auth-next',
    '@nuxtjs/router',
  ],

  auth: {
    redirect: {
      login: '/login',
      logout: '/login',
      home: '/'
    },
    strategies: {
      // eslint-disable-next-line quote-props
      'laravelSanctum': {
        provider: 'laravel/sanctum',
        url: 'https://api.client.test',
        endpoints: {
          login: {
            url: '/api/login',
            method: 'post'
          },
          logout: {
            url: '/api/logout',
            method: 'post'
          },
          user: {
            url: '/api/user',
            method: 'get'
          }
        },
        user: {
          autoFetch: true
        }
      }
    },
    localStorage: {
      prefix: 'auth.'
    },
    cookie: {
      prefix: 'auth.', // Default token prefix used in building a key for token storage in the browser's localStorage.
      options: {
        path: '/', // Path where the cookie is visible. Default is '/'.
        expires: 5 // Can be used to specify cookie lifetime in Number of days or specific Date. Default is session only.
      }
    }
  },

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    baseURL: 'http://localhost:8000',
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
  }
}
